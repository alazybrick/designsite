import React from 'react'
import Link from 'gatsby-link'

const IndexPage = () => (
  <div className="container">
   <div className="content">
    <div className="header">
      <h1><span id="logo">Pas de nom</span> — graphic design <br></br>
        team based in germany.</h1>
    </div>
    <div>
      <p>Two buddies from the same town with the same interest. Creating User Interfaces and Experiences, for mobile aswell as web. When we’re talking design, we tend to overthink everything, creating stuff that no one really asked for. But always with a happy face.</p>
    </div>
    <div className="btns">
      <a href="https://dribbble.com/alazybrick" target="_blank"><div id="btn_d">
        <h2><span id="dribbble">see our latest work on </span>dribbble</h2> 
        <h2 className="emoji">🚀</h2>
      </div></a>
      <a href="https://dribbble.com/alazybrick" target="_blank"><div id="btn_t">
        <h2><span id="twitter">follow us on </span>twitter</h2>
        <h2 className="emoji">🐤</h2>
      </div></a>
    </div>
    </div>
  </div>
)

export default IndexPage
